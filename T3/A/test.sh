#/bin/bash

while getopts "c:d:o:" OPTION
do
    case $OPTION in
    c) SINGLE_TEST=1
        n_test=$OPTARG
        ;;
    d) DIR_FLAG=1
        dir=$OPTARG
        ;;
    o) OUTPUT_FILE=1
        file=$OPTARG
        ;;
    ?) echo " Argumento inválido."
        exit 1
        ;;
    esac
done
shift $((OPTIND-1))

echo "Digite o nome do arquivo executável: "
read ex

if [[ ! -x $ex ]];
then
    echo "Arquivo executável não existe."
    exit 1
fi

if [ "$DIR_FLAG" == 1 ];
then
    if [[ ! -d $dir ]];
    then
        echo $dir "não é um diretório válido."
        exit 1
    fi
fi

if [ "$SINGLE_TEST" == 1 ];    
then
    if [ $n_test -lt 0 ] || [ $n_test -gt 30 ];
    then
        echo "Caso de teste deve ser um valor entre 0 e 30"
        exit 1
    fi

    if [ "$DIR_FLAG" == 1 ];
    then
        ./$ex < "$dir"/$n_test.in > $ex$n_test.out
        diff $ex$n_test.out "$dir"/$n_test.out > diff

        if [ -s diff ];
        then
            echo "***************************************************"
            echo "                   Caso" $n_test
            echo
            cat diff
            echo "***************************************************"
        else
            echo "Caso" $n_test "- OK"
        fi
    else
        ./$ex < $n_test.in > $ex$n_test.out
        diff $ex$n_test.out $n_test.out > diff

        if [ -s diff];
        then
            echo "***************************************************"
            echo "                   Caso" $n_test
            echo
            cat diff
            echo "***************************************************"
        else
            echo "Caso" $n_test "- OK"
        fi
    fi
    
    if [ "$OUTPUT_FILE" == 1 ];
    then
        mv $ex$n_test.out $file
    else
        rm $ex$n_test.out
    fi
   
    rm diff
else
    for i in {0..10};
    do
        if [ "$DIR_FLAG" == 1 ];
        then
            ./$ex < "$dir"/$i.in > ${ex}temp.out
            diff ${ex}temp.out "$dir"/$i.out > diff

            if [ -s diff ];
            then
                echo "***************************************************"
                echo "                   Caso" $i
                echo
                cat diff
                echo "***************************************************"
                echo
                rm diff ${ex}temp.out 
                exit 1
            else
                echo "Caso" $i "- OK"
            fi
        else
            ./$ex < $i.in > ${ex}temp.out
            diff ${ex}temp.out $i.out > diff

            if [ -s diff];
            then
                echo "***************************************************"
                echo "                   Caso" $i
                echo
                cat diff
                echo "***************************************************"
            else
                echo "Caso" $i "- OK"
            fi
        fi
    done
    rm diff ${ex}temp.out
fi