/* ==========================================================================
 * Universidade Federal de São Carlos - Campus Sorocaba
 * Disciplina: Estruturas de Dados 2
 * Prof. Tiago A. de Almeida
 *
 * Trabalho 03A - Hashing com reespalhamento linear
 *
 * RA: 743570	
 * Aluno: Luiz Felipe Guimarães
 * ========================================================================== */

 /* Bibliotecas */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>

/* Tamanho dos campos dos registros */
#define TAM_PRIMARY_KEY 11
#define TAM_NOME 51
#define TAM_MARCA 51
#define TAM_DATA 11
#define TAM_ANO 3
#define TAM_PRECO 8
#define TAM_DESCONTO 4
#define TAM_CATEGORIA 51

#define TAM_REGISTRO 192
#define MAX_REGISTROS 1000
#define TAM_ARQUIVO (MAX_REGISTROS * TAM_REGISTRO + 1)


#define POS_OCUPADA 	"[%d] Ocupado: %s\n"
#define POS_LIVRE 		"[%d] Livre\n"
#define POS_REMOVIDA 	"[%d] Removido\n"

/* Estado das posições da tabela hash */
#define LIVRE       0
#define OCUPADO     1
#define REMOVIDO    2

/* Saídas do usuário */
#define OPCAO_INVALIDA "Opcao invalida!\n"
#define MEMORIA_INSUFICIENTE "Memoria insuficiente!\n"
#define REGISTRO_N_ENCONTRADO "Registro(s) nao encontrado!\n"
#define CAMPO_INVALIDO "Campo invalido! Informe novamente.\n"
#define ERRO_PK_REPETIDA "ERRO: Ja existe um registro com a chave primaria: %s.\n\n"
#define ARQUIVO_VAZIO "Arquivo vazio!"
#define INICIO_BUSCA 							 "********************************BUSCAR********************************\n"
#define INICIO_LISTAGEM						  "********************************LISTAR********************************\n"
#define INICIO_ALTERACAO 						"********************************ALTERAR*******************************\n"
#define INICIO_ARQUIVO					    "********************************ARQUIVO*******************************\n"
#define INICIO_EXCLUSAO 			 			"**********************EXCLUIR*********************\n"

#define SUCESSO  				 "OPERACAO REALIZADA COM SUCESSO!\n"
#define FALHA 					 "FALHA AO REALIZAR OPERACAO!\n"
#define ERRO_TABELA_CHEIA 		"ERRO: Tabela Hash esta cheia!\n\n"
#define REGISTRO_INSERIDO 		"Registro %s inserido com sucesso. Numero de colisoes: %d.\n\n"


/* Registro do jogo */
typedef struct {
	char pk[TAM_PRIMARY_KEY];
	char nome[TAM_NOME];
	char marca[TAM_MARCA];
	char data[TAM_DATA];	/* DD/MM/AAAA */
	char ano[TAM_ANO];
	char preco[TAM_PRECO];
	char desconto[TAM_DESCONTO];
	char categoria[TAM_CATEGORIA];
} Jogo;

/* Registro da Tabela Hash
 * Contém o estado da posição, a chave primária e o RRN do respectivo registro */
typedef struct {
	int estado;
	char pk[TAM_PRIMARY_KEY];
	int rrn;
} Chave;

/* Estrutura da Tabela Hash */
typedef struct {
    int tam;
    Chave *v;
  } Hashtable;

  /* Variáveis globais */
char ARQUIVO[TAM_ARQUIVO];
int nregistros;




/* ==========================================================================
 * ========================= PROTÓTIPOS DAS FUNÇÕES =========================
 * ========================================================================== */


/* Recebe do usuário uma string simulando o arquivo completo. */
void carregar_arquivo();

/* Exibe o jogo */
int exibir_registro(int rrn);

/*Função de Hash*/
short hash(const char* chave, int tam);


/*Auxiliar para a função de hash*/
short f(char x);

/*Retorna o primeiro número primo >= a*/
int  prox_primo(int a);

/*Funções do Menu*/
void carregar_tabela(Hashtable* tabela);
void cadastrar(Hashtable* tabela);
int  alterar(Hashtable tabela);
void buscar(Hashtable tabela);
int  remover(Hashtable* tabela);
void liberar_tabela(Hashtable* tabela);
int inserir_chave(char *chave, Hashtable *tabela, int rrn);

/* <<< DECLARE AQUI OS PROTOTIPOS >>> */
Jogo recuperar_registro(int rrn);
void gerarChave(Jogo *prod);
void criar_tabela(Hashtable *tabela, int tam);
Chave* buscar_aux(char *chave, Hashtable *tabela);
void imprimir_tabela(Hashtable tabela);


/* ==========================================================================
 * ============================ FUNÇÃO PRINCIPAL ============================
 * =============================== NÃO ALTERAR ============================== */
int main() 
{    
	/* Arquivo */
	int carregarArquivo = 0;
	scanf("%d%*c", &carregarArquivo); // 1 (sim) | 0 (nao)
	if (carregarArquivo) 
		carregar_arquivo();

	/* Tabela Hash */
	int tam;
	scanf("%d%*c", &tam);
	tam = prox_primo(tam);

	Hashtable tabela;
	criar_tabela(&tabela, tam);
	if (carregarArquivo) 
		carregar_tabela(&tabela);

	/* Execução do programa */
	int opcao = 0;
	while(opcao != 6) {
			scanf("%d%*c", &opcao);
			switch(opcao) 
			{
				case 1:
					cadastrar(&tabela);
					break;
				case 2:
					printf(INICIO_ALTERACAO);
					if(alterar(tabela))
						printf(SUCESSO);
					else
						printf(FALHA);
					break;
				case 3:
					printf(INICIO_BUSCA);
					buscar(tabela);
					break;
				case 4:
					printf(INICIO_EXCLUSAO);
					if(	remover(&tabela))
						printf(SUCESSO);
					else
						printf(FALHA);
					break;
				case 5:
					printf(INICIO_LISTAGEM);
					imprimir_tabela(tabela);
					break;
				case 6:
					liberar_tabela(&tabela);
					break;
				case 10:
					printf(INICIO_ARQUIVO);
					printf("%s\n", (*ARQUIVO!='\0') ? ARQUIVO : ARQUIVO_VAZIO);
					break;
				default:
					printf(OPCAO_INVALIDA);
					break;
			}
	}
	return 0;
}

/* <<< IMPLEMENTE AQUI AS FUNCOES >>> */

/* Recebe do usuário uma string simulando o arquivo completo. */
void carregar_arquivo() {
	scanf("%[^\n]\n", ARQUIVO);
	nregistros = strlen(ARQUIVO) / TAM_REGISTRO;
}

/*Auxiliar para a função de hash*/
short f(char x)
{
	return (x < 59) ? x - 48 : x - 54; 
}

/* Exibe o jogo */
int exibir_registro(int rrn)
{
	if(rrn<0)
		return 0;
	float preco;
	int desconto;
	Jogo j = recuperar_registro(rrn);
  char *cat, categorias[TAM_CATEGORIA] = {'\0'};
	printf("%s\n", j.pk);
	printf("%s\n", j.nome);
	printf("%s\n", j.marca);
	printf("%s\n", j.data);
	printf("%s\n", j.ano);
	sscanf(j.desconto,"%d",&desconto);
	sscanf(j.preco,"%f",&preco);
	preco = preco *  (100-desconto);
	preco = ((int) preco)/ (float) 100 ;
	printf("%07.2f\n",  preco);
	strncpy(categorias, j.categoria, strlen(j.categoria));
  for (cat = strtok (categorias, "|"); cat != NULL; cat = strtok (NULL, "|"))
    printf("%s ", cat);
	printf("\n");
	return 1;
}

Jogo recuperar_registro(int rrn) {
	char temp[193] = {'\0'}, *p;
	strncpy(temp, ARQUIVO + ((rrn)*192), 192);
	Jogo j;
	p = strtok(temp, "@");
	strcpy(j.pk, p);
	strcat(j.pk, "\0");
	p = strtok(NULL,"@");
	strcpy(j.nome,p);
	strcat(j.nome, "\0");
	p = strtok(NULL,"@");
	strcpy(j.marca,p);
	strcat(j.marca, "\0");
	p = strtok(NULL,"@");
	strcpy(j.data,p);
	strcat(j.data, "\0");
	p = strtok(NULL,"@");
	strcpy(j.ano,p);
	strcat(j.ano, "\0");
	p = strtok(NULL,"@");
	strcpy(j.preco,p);
	strcat(j.preco, "\0");
	p = strtok(NULL,"@");
	strcpy(j.desconto,p);
	strcat(j.desconto, "\0");
	p = strtok(NULL,"@");
	strcpy(j.categoria,p);
	strcat(j.categoria, "\0");;
	return j;
}

void gerarChave(Jogo *prod) {
	prod->pk[0] = '\0';
	strncat(prod->pk, prod->nome, 2);
	strncat(prod->pk, prod->marca, 2);
	strncat(prod->pk, prod->data, 2); // dia
	strncat(prod->pk, prod->data + 3, 2); // mes (pos. 3)
	strcat(prod->pk, prod->ano);
}

int prox_primo(int a) {
	int aux;

	do {
		aux = 1;
		do {
			aux++;
		} while (a % aux);
		if (aux == a)
			return a;
	} while (a++);
}

short hash(const char* chave, int tam) {
	short hash = 0;

	for (int i = 0; i < 8; i++) {
		hash += (i+1) * f(chave[i]);
	}

	return hash % tam;
}

void criar_tabela(Hashtable *tabela, int tam) {
	tabela->tam = tam;
	tabela->v = (Chave*) malloc(sizeof(Chave) * tam);

	for (int i = 0; i < tam; i++) {
		tabela->v[i].estado = LIVRE;
		tabela->v[i].rrn = -1;
		strcat(tabela->v[i].pk, "\0");
	}
}

void carregar_tabela(Hashtable *tabela) {
	Jogo j;
	for (int i = 0; i < nregistros; i++) {
		j = recuperar_registro(i);
		inserir_chave(j.pk, tabela, i);
	}
}

int inserir_chave(char *chave, Hashtable *tabela, int rrn) {
	short pos = hash(chave, tabela->tam);
	int colisoes = 0;

	// Verifica se a posicao esta livre
	if (tabela->v[pos].estado != OCUPADO) {
		tabela->v[pos].estado = OCUPADO;
		tabela->v[pos].rrn = rrn;
		strcpy(tabela->v[pos].pk, chave);
	// Senao procura a proxima posicao livre na tabela hash
	} else {
		int flag = 0;
		for (int i = pos + 1; i % tabela->tam != pos && !flag; i++) {
			i %= tabela->tam;
			colisoes++;
			if (tabela->v[i].estado != OCUPADO) {
				flag = 1;
				tabela->v[i].estado = OCUPADO;
				tabela->v[i].rrn = rrn;
				strcpy(tabela->v[i].pk, chave);
			}
		}

		if (!flag) {
			return -1;
		}
	}

	return colisoes;
}

void cadastrar(Hashtable* tabela) {
	char registro[TAM_REGISTRO + 1] = {'\0'};
	Jogo j;

	scanf("%[^\n]%*c", j.nome);
	scanf("%[^\n]%*c", j.marca);
	scanf("%[^\n]%*c", j.data);
	scanf("%[^\n]%*c", j.ano);
	scanf("%[^\n]%*c", j.preco);
	scanf("%[^\n]%*c", j.desconto);
	scanf("%[^\n]%*c", j.categoria);
	gerarChave(&j);

	Chave *busca = buscar_aux(j.pk, tabela);
	
	// Se chave primaria ja existir
	if (busca) {
		printf(ERRO_PK_REPETIDA, j.pk);
		return;
	}

	registro[0] = '\0';

	strcat(registro, j.pk);
	strcat(registro, "@");
	strcat(registro, j.nome);
	strcat(registro, "@");
	strcat(registro, j.marca);
	strcat(registro, "@");
	strcat(registro, j.data);
	strcat(registro, "@");
	strcat(registro, j.ano);
	strcat(registro, "@");
	strcat(registro, j.preco);
	strcat(registro, "@");
	strcat(registro, j.desconto);
	strcat(registro, "@");
	strcat(registro, j.categoria);
	strcat(registro, "@");

	int tam = strlen(registro);
	for (int i = 0; i < (TAM_REGISTRO - tam); i++) {
		strcat(registro, "#");
	}


	// Insere a chave na tabela hash
	int colisoes = inserir_chave(j.pk, tabela, nregistros);
	if (colisoes != -1) {
		strcat(ARQUIVO + (TAM_REGISTRO * nregistros), registro);
		printf(REGISTRO_INSERIDO, j.pk, colisoes);
		nregistros++;
	} else {
		printf(ERRO_TABELA_CHEIA);
	}
}

int alterar(Hashtable tabela) {
	char chave[TAM_PRIMARY_KEY + 1] = "\0", desconto[TAM_DESCONTO + 1] = "\0", registro[TAM_REGISTRO + 1] = "\0";
	Chave *busca;

	scanf("%[^\n]%*c", chave);

	busca = buscar_aux(chave, &tabela);

	if (!busca) {
		printf(REGISTRO_N_ENCONTRADO);
		return 0;
	}
	
	scanf("%[^\n]%*c", desconto);

	// Verificacoes de integridade
	int desconto_int = atoi(desconto);
	while (strlen(desconto) != 3 || desconto_int < 0 || desconto_int > 100) {
		printf(CAMPO_INVALIDO);
		scanf("%[^\n]%*c", desconto);
		desconto_int = atoi(desconto);
	}
	
	// Se passou das verificacoes, aplica a alteracao
	Jogo j = recuperar_registro(busca->rrn);

	strcat(registro, j.pk);
	strcat(registro, "@");
	strcat(registro, j.nome);
	strcat(registro, "@");
	strcat(registro, j.marca);
	strcat(registro, "@");
	strcat(registro, j.data);
	strcat(registro, "@");
	strcat(registro, j.ano);
	strcat(registro, "@");
	strcat(registro, j.preco);
	strcat(registro, "@");
	strcat(registro, desconto);
	strcat(registro, "@");
	strcat(registro, j.categoria);
	strcat(registro, "@");

	int tam = strlen(registro);
	for (int i = 0; i < (TAM_REGISTRO - tam); i++) {
		strcat(registro, "#");
	}

	strncpy(ARQUIVO + (busca->rrn * TAM_REGISTRO), registro, TAM_REGISTRO);

	return 1;
}

Chave* buscar_aux(char *chave, Hashtable *tabela) {
	short pos = hash(chave, tabela->tam);
	// Verifica se existe chave naquela posicao
	if (tabela->v[pos].estado == OCUPADO) {
		// Verifica se a chave na posicao eh a mesma que a chave procurada
		if (!strcmp(tabela->v[pos].pk, chave)) {
			return tabela->v + pos;
		}
	}

	/* Percorre a tabela hash procurando a chave, para caso a chave tenha sido inserida
		em outra posicao devido a colisoes
	*/
	if (tabela->v[pos].estado != LIVRE) {	// Impossivel haver colisao com posicao livre
		// Percorre o vetor de forma circular
		for (int i = pos + 1; i % tabela->tam != pos; i++) {
			i %= tabela->tam;
			if (tabela->v[i].estado == OCUPADO) {
				if (!strcmp(tabela->v[i].pk, chave)) {
					return tabela->v + i;
				}
			}
		}
	}

	// Chave nao encontrada
	return NULL;
}

void buscar(Hashtable tabela) {
	char chave[TAM_PRIMARY_KEY + 1] = {'\0'};
	Chave *busca;

	scanf("%[^\n]%*c", chave);

	busca = buscar_aux(chave, &tabela);

	if (busca) {
		exibir_registro(busca->rrn);
	} else {
		printf(REGISTRO_N_ENCONTRADO);
	}

}

int remover(Hashtable* tabela) {
	char chave[TAM_PRIMARY_KEY + 1], *p_arq;
	Chave *busca;
	
	scanf ("%[^\n]%*c", chave);
	busca = buscar_aux(chave, tabela);

	// Verifica se registro existe
	if (!busca) {
		printf(REGISTRO_N_ENCONTRADO);
		return 0;
	}

	// Marca o inicio do registro no arquivo
	p_arq = ARQUIVO + busca->rrn * TAM_REGISTRO;
	strncpy(p_arq, "*|", 2);
	
	// Remove chave da tabela hash
	busca->estado = REMOVIDO;
	busca->rrn = -1;
	strcat(busca->pk, "\0");

	return 1;
}

void liberar_tabela(Hashtable *tabela) {
	free(tabela->v);
}

void imprimir_tabela(Hashtable tabela) {
	for (int i = 0; i < tabela.tam; i++) {
		switch(tabela.v[i].estado) {
			case LIVRE:
				printf(POS_LIVRE, i);
				break;
			case OCUPADO:
				printf(POS_OCUPADA, i, tabela.v[i].pk);	
				break;
			case REMOVIDO:
				printf(POS_REMOVIDA, i);
		}
	}
}