/* ==========================================================================
 * Universidade Federal de São Carlos - Campus Sorocaba
 * Disciplina: Estruturas de Dados 2
 * Prof. Tiago A. de Almeida
 *
 * Trabalho 01
 *
 * RA: 743570
 * Aluno: Luiz Felipe Guimarães
 * ========================================================================== */

/* Bibliotecas */
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>


/* Tamanho dos campos dos registros */
#define TAM_PRIMARY_KEY	11
#define TAM_NOME 		51
#define TAM_MARCA 		51
#define TAM_DATA 		11
#define TAM_ANO 		3
#define TAM_PRECO 		8
#define TAM_DESCONTO 	4
#define TAM_CATEGORIA 	51


#define TAM_REGISTRO 	192
#define MAX_REGISTROS 	1000
#define MAX_CATEGORIAS 	30
#define TAM_ARQUIVO (MAX_REGISTROS * TAM_REGISTRO + 1)


/* Saídas para o usuario */
#define OPCAO_INVALIDA 				"Opcao invalida!\n"
#define MEMORIA_INSUFICIENTE 		"Memoria insuficiente!"
#define REGISTRO_N_ENCONTRADO 		"Registro(s) nao encontrado!\n"
#define CAMPO_INVALIDO 				"Campo invalido! Informe novamente.\n"
#define ERRO_PK_REPETIDA			"ERRO: Ja existe um registro com a chave primaria: %s.\n"
#define ARQUIVO_VAZIO 				"Arquivo vazio!\n"
#define INICIO_BUSCA 		 		"**********************BUSCAR**********************\n"
#define INICIO_LISTAGEM  			"**********************LISTAR**********************\n"
#define INICIO_ALTERACAO 			"**********************ALTERAR*********************\n"
#define INICIO_EXCLUSAO  			"**********************EXCLUIR*********************\n"
#define INICIO_ARQUIVO  			"**********************ARQUIVO*********************\n"
#define INICIO_ARQUIVO_SECUNDARIO	"*****************ARQUIVO SECUNDARIO****************\n"
#define SUCESSO  				 	"OPERACAO REALIZADA COM SUCESSO!\n"
#define FALHA 					 	"FALHA AO REALIZAR OPERACAO!\n"



/* Registro do Produto */
typedef struct {
	char pk[TAM_PRIMARY_KEY];
	char nome[TAM_NOME];
	char marca[TAM_MARCA];
	char data[TAM_DATA];	/* DD/MM/AAAA */
	char ano[TAM_ANO];
	char preco[TAM_PRECO];
	char desconto[TAM_DESCONTO];
	char categoria[TAM_CATEGORIA];
} Produto;


/*----- Registros dos Índices -----*/

/* Struct para índice Primário */
typedef struct primary_index{
  char pk[TAM_PRIMARY_KEY];
  int rrn;
} Ip;

/* Struct para índice secundário */
typedef struct secundary_index{
  char pk[TAM_PRIMARY_KEY];
  char string[TAM_NOME];
} Is;

/* Struct para índice secundário de preços */
typedef struct secundary_index_of_final_price{
  float price;
  char pk[TAM_PRIMARY_KEY];
} Isf;

/* Lista ligada para o Índice abaixo*/
typedef struct linked_list{
  char pk[TAM_PRIMARY_KEY];
  struct linked_list *prox;
} ll; 

/* Struct para lista invertida */
typedef struct reverse_index{
  char cat[TAM_CATEGORIA];
  ll* lista;
} Ir;

/*----- GLOBAL -----*/
char ARQUIVO[TAM_ARQUIVO];

/* ==========================================================================
 * ========================= PROTÓTIPOS DAS FUNÇÕES =========================
 * ========================================================================== */

/* Recebe do usuário uma string simulando o arquivo completo e retorna o número
 * de registros. */
int carregar_arquivo();

/* Exibe o Produto */
int exibir_registro(int rrn, char com_desconto);

/* Recupera do arquivo o registro com o rrn informado
 *  e retorna os dados na struct Produto */
Produto recuperar_registro(int rrn);

/* (Re)faz o índice respectivo */
void criar_iprimary(Ip *indice_primario, int* nregistros);

/* Realiza os scanfs na struct Produto */
void ler_entrada(char* registro, Produto *novo);

/* Rotina para impressao de indice secundario */
void imprimirSecundario(Is* iproduct, Is* ibrand, Ir* icategory, Isf *iprice, int nregistros, int ncat);

/* Gera a chave primária de um registro */
void gerarChave(Produto *prod);

/* (Re)constroi indice de nome ou modelo dos produtos a partir do indice primario */
void criar_iproduct(Is *indice_nome, Ip *iprimary, int *nregistros);

/* Compara dois Ip e retorna 1 se o primeiro for menor (em ordem lexografica do codigo),
   -1 se o segundo for menor ou 0 se ambos forem iguais (nao deve acontecer).
   Necessaria para usar qsort() 
*/
int comp_iprimary(const void *p1, const void *p2);

/* Compara dois registros de indice secundario e retorna o menor (ordem lecografica),
   usando a chave primaria como desempate 
*/  
int comp_isecondary(const void *p1, const void *p2);

/* (Re)constroi indice de marca dos produtos a partir do indice primario */
void criar_ibrand(Is *indice_marca, Ip *iprimary, int *nregistros);

/* Compara dois registros de indice de preco final e retorna o menor,
   usando a chave primaria como desempate
*/
int comp_iprice(const void *p1, const void *p2);

/*(Re)constroi indice de preco final dos produtos a partir do indice primario */
void criar_iprice(Isf *indice_preco, Ip *iprimary, int *nregistros);

/* Construtor da lista. */
void inicializar_lista(ll **head);

/* Insere um novo elemento na lista.
   Retorna 1 se inserido ou 0 se memoria insuficiente
*/
int inserir_lista(char *chave, ll **head);

/* Destroi lista e libera memoria */
void destruir_lista(ll **head);

/* Compara dois registros de indice de categoria e retorna o menor
*/
int comp_icategory(const void *p1, const void *p2);


/* (Re)constroi indice de categorias dos produtos a partir do indice primario */
void criar_icategory(Ir *indice_categoria, Ip *iprimary, int *nregistros, int *ncat);

/* Cadastra um novo produto no arquivo de dados e atualiza os indices */
void cadastrar(Ip *iprimary, Is *iproduct, Is *ibrand, Isf *iprice, Ir *icategory, int *nregistros, int *ncat);

/* Lista os registros formatados e ordenados por: (1) código, (2) categoria, (3) marca
   (4) preco com desconto aplicado
*/
void listar(Ip* iprimary, Is* iproduct, Is* ibrand, Ir* icategory, Isf *iprice, int nregistros, int ncat);

/* Procura por registro com a chave desejada no indice de categorias.
   Retorna ponteiro para registro caso encontre ou NULL caso contrario.
*/
Ir *buscar_categoria(Ir *indice_categoria, char *chave, int ncat);

/* Procura por registro com a chave desejada no indice de nome (ou modelo).
   Retorna ponteiro para registro caso encontre ou NULL caso contrario.
*/
Is *buscar_modelo(Is *iproduct, char *chave, int nregistros);

/* Procura por registro com a chave desejada no indice de marca.
   Retorna ponteiro para registro caso encontre ou NULL caso contrario.
*/
Is *buscar_marca(Is *ibrand, char *chave, int nregistros);

/* Busca um registro por: (1) código, (2) nome ou modelo ou (3) marca e categoria.
   Mostra o produto formatado na tela se for encontrado ou mensagem de registro
   nao encontrado caso contrario
*/
void buscar(Ip *iprimary, Is *iproduct, Is *ibrand, Ir *icategory, int nregistros, int ncat);

/* Altera o desconto de um produto, este deve ter tamanho de 3 bytes e valor entre 0 e 100 */
int alterar(Ip* iprimary, Isf* iprice, int *nregistros);

/* Remove um produto marcando -1 no indice primario, o registro continua no arquivo
   e na tabela de indices
*/
int remover(Ip* iprimary, int nregistros);


/* Remove todos os registros apagados do arquivo e refaz as tabelas de indices */
void liberar_espaco(Ip *iprimary, Is* iproduct, Is *ibrand, Isf *iprice, Ir *icategory, int *nregistros, int *ncat);

/* Libera a memoria principal e encerra o programa*/
void finalizar(Ip *iprimary, Is *iproduct, Is *ibrand, Isf *iprice, Ir *icategory, int ncat);

/* ==========================================================================
 * ============================ FUNÇÃO PRINCIPAL ============================
 * =============================== NÃO ALTERAR ============================== */
int main(){
  /* Arquivo */
	int carregarArquivo = 0, nregistros = 0, ncat = 0;
	scanf("%d%*c", &carregarArquivo); /* 1 (sim) | 0 (nao) */
	if (carregarArquivo)
		nregistros = carregar_arquivo();

	/* Índice primário */
	Ip *iprimary = (Ip *) malloc (MAX_REGISTROS * sizeof(Ip));
  	if (!iprimary) {
		perror(MEMORIA_INSUFICIENTE);
		exit(1);
	}
	criar_iprimary(iprimary, &nregistros);

	/*Alocar e criar índices secundários*/
	/* Indice para nome ou modelo dos produtos */
	Is *iproduct = (Is *) malloc(MAX_REGISTROS * sizeof(Is));
	if (!iproduct) {
		perror(MEMORIA_INSUFICIENTE);
		exit(1);
	}
	criar_iproduct(iproduct, iprimary, &nregistros);

	/* Indice para marca dos produtos */
	Is *ibrand = (Is *) malloc(MAX_REGISTROS * sizeof(Is));
	if (!ibrand) {
		perror(MEMORIA_INSUFICIENTE);
		exit(1);
	}
	criar_ibrand(ibrand, iprimary, &nregistros);

	/* Indice para categorias dos produtos */
	Ir *icategory = (Ir *) malloc(MAX_CATEGORIAS * sizeof(Ir));
	if (!icategory) {
		perror(MEMORIA_INSUFICIENTE);
		exit(1);
	}
	criar_icategory(icategory, iprimary, &nregistros, &ncat);

	/* Indice para preço final dos produtos */
	Isf *iprice = (Isf *) malloc(MAX_REGISTROS * sizeof(Isf));
	if (!iprice) {
		perror(MEMORIA_INSUFICIENTE);
		exit(1);
	}
	criar_iprice(iprice, iprimary, &nregistros);

	/* Execução do programa */
	int opcao = 0;
	while(1)
	{
		scanf("%d%*c", &opcao);
		switch(opcao)
		{
			case 1:
				/*cadastro*/
				cadastrar(iprimary, iproduct, ibrand, iprice, icategory, &nregistros, &ncat);
			break;
			case 2:
				/*alterar desconto*/
				printf(INICIO_ALTERACAO);
				if(alterar(iprimary, iprice, &nregistros))
					printf(SUCESSO);
				else
					printf(FALHA);
			break;
			case 3:
				/*excluir produto*/
				printf(INICIO_EXCLUSAO);
				if(remover(iprimary, nregistros))
					printf(SUCESSO);
				else
					printf(FALHA);
			break;
			case 4:
				/*busca*/
				printf(INICIO_BUSCA );
				buscar(iprimary, iproduct, ibrand, icategory, nregistros, ncat);
			break;
			case 5:
				/*listagens*/
				printf(INICIO_LISTAGEM);
				listar(iprimary, iproduct, ibrand, icategory, iprice, nregistros, ncat);
			break;
			case 6:
				/*libera espaço*/
				liberar_espaco(iprimary, iproduct, ibrand, iprice, icategory, &nregistros, &ncat);
			break;
			case 7:
				/*imprime o arquivo de dados*/
				if (strlen(ARQUIVO)) {
					printf(INICIO_ARQUIVO);
					printf("%s\n", ARQUIVO);
				} else {
					printf(INICIO_ARQUIVO);
					printf(ARQUIVO_VAZIO);
				}
			break;
			case 8:
				/*imprime os índices secundários*/
				imprimirSecundario(iproduct, ibrand, icategory, iprice, nregistros, ncat);
			break;
			case 9:
	      		/*Liberar memória e finalizar o programa */
				  finalizar(iprimary, iproduct, ibrand, iprice, icategory, ncat);
				return 0;
			break;
			default:
				printf(OPCAO_INVALIDA);
			break;
		}
	}
	return 0;
}


/* Exibe o Produto */
int exibir_registro(int rrn, char com_desconto)
{
	if(rrn<0)
		return 0;
	float preco;
	int desconto;
	Produto j = recuperar_registro(rrn);
  	char *cat, categorias[TAM_CATEGORIA + 1];
	printf("%s\n", j.pk);
	printf("%s\n", j.nome);
	printf("%s\n", j.marca);
	printf("%s\n", j.data);
	if(!com_desconto)
	{
		printf("%s\n", j.preco);
		printf("%s\n", j.desconto);
	}
	else
	{
		sscanf(j.desconto,"%d",&desconto);
		sscanf(j.preco,"%f",&preco);
		preco = preco *  (100-desconto);
		preco = ((int) preco)/ (float) 100;
		printf("%07.2f\n",  preco);

	}
	strcpy(categorias, j.categoria);

	cat = strtok (categorias, "|");

	while(cat != NULL){
		printf("%s", cat);
		cat = strtok (NULL, "|");
		if(cat != NULL){
			printf(", ");
		}
	}

	printf("\n");

	return 1;
}


int carregar_arquivo()
{
	scanf("%[^\n]\n", ARQUIVO);
	return strlen(ARQUIVO) / TAM_REGISTRO;
}


/* Recupera do arquivo o registro com o rrn
 * informado e retorna os dados na struct Produto */
Produto recuperar_registro(int rrn)
{
	char temp[193], *p;
	strncpy(temp, ARQUIVO + ((rrn)*192), 192);
	temp[192] = '\0';
	Produto j;
	p = strtok(temp,"@");
	strcpy(j.nome,p);
	strcat(j.nome, "\0");
	p = strtok(NULL,"@");
	strcpy(j.marca,p);
	strcat(j.marca, "\0");
	p = strtok(NULL,"@");
	strcpy(j.data,p);
	strcat(j.data, "\0");
	p = strtok(NULL,"@");
	strcpy(j.ano,p);
	strcat(j.ano, "\0");
	p = strtok(NULL,"@");
	strcpy(j.preco,p);
	strcat(j.preco, "\0");
	p = strtok(NULL,"@");
	strcpy(j.desconto,p);
	strcat(j.desconto, "\0");
	p = strtok(NULL,"@");
	strcpy(j.categoria,p);
	strcat(j.categoria, "\0");;
	gerarChave(&j);
	return j;
}


/* Imprimir indices secundarios */
void imprimirSecundario(Is* iproduct, Is* ibrand, Ir* icategory, Isf *iprice, int nregistros, int ncat){
	int opPrint = 0;
	ll *aux;
	printf(INICIO_ARQUIVO_SECUNDARIO);
	scanf("%d", &opPrint);
	if(!nregistros)
		printf(ARQUIVO_VAZIO);
	switch (opPrint) {
		case 1:
			for(int i = 0; i < nregistros; i++){
				printf("%s %s\n",iproduct[i].pk, iproduct[i].string);
			}
		break;
		case 2:
			for(int i = 0; i < nregistros; i++){
				printf("%s %s\n",ibrand[i].pk, ibrand[i].string);
			}
		break;
		case 3:
			for(int i = 0; i < ncat; i++){
				printf("%s", icategory[i].cat);
				aux =  icategory[i].lista;
				while(aux != NULL) { 
					printf(" %s", aux->pk);
					aux = aux->prox;
				}
				printf("\n");
			}
		break;

		case 4:
		for(int i = 0; i < nregistros; i++){
			printf("%s %.2f\n",iprice[i].pk, iprice[i].price);
		}
		break;
	}
}

/* Gerar chave primaria */
void gerarChave(Produto *prod) {
	prod->pk[0] = '\0';
	strncat(prod->pk, prod->nome, 2);
	strncat(prod->pk, prod->marca, 2);
	strncat(prod->pk, prod->data, 2); // dia
	strncat(prod->pk, prod->data + 3, 2); // mes (pos. 3)
	strcat(prod->pk, prod->ano);
}

/* Compara dois registros de indice primario */
int comp_iprimary(const void *p1, const void *p2) {
	return strcmp((*(Ip*)p1).pk, (*(Ip*)p2).pk);
}

/* Constroi tabela de indices primarios */
void criar_iprimary(Ip *indice_primario, int* nregistros) {
	Produto j;

	for (int i = 0; i < *nregistros; i++) {
		j = recuperar_registro(i);
		strcpy(indice_primario[i].pk, j.pk);
		// Se registro nao estiver apagado
		if (strncmp(j.nome, "*|", 2) != 0) {
			indice_primario[i].rrn = i;
		// Marca rrn como -1 caso registro esteja apagado
		} else {
			indice_primario[i].rrn = -1;
		}
	}

	qsort(indice_primario, *nregistros, sizeof(Ip), comp_iprimary);
}

/* Compara dois registros de indice secundario de nome e retorna o menor,
   usando a chave primaria como desempate 
*/  
int comp_isecondary(const void *p1, const void *p2) {
	int res = strcmp((*(Is*)p1).string, (*(Is*)p2).string);

	// Se forem iguais, retorna a menor pk
	if (!res) { 
		return strcmp((*(Ip*)p1).pk, (*(Ip*)p2).pk);
	} else {
		return res;
	}
}

/* Constroi tabela de indices de nome ou modelo dos produtos a partir do indice primario */
void criar_iproduct(Is *indice_nome, Ip *iprimary, int *nregistros) {
	Produto j;

	for (int i = 0; i < *nregistros; i++) {
		j = recuperar_registro(iprimary[i].rrn);
		strcpy(indice_nome[i].string, j.nome);
		strcpy(indice_nome[i].pk, j.pk);
	}

	qsort(indice_nome, *nregistros, sizeof(Is), comp_isecondary);
}

/* (Re)constroi indice de marca dos produtos a partir do indice primario */
void criar_ibrand(Is *indice_marca, Ip *iprimary, int *nregistros) {
	Produto j;

	for (int i = 0; i < *nregistros; i++) {
		j = recuperar_registro(iprimary[i].rrn);
		strcpy(indice_marca[i].string, j.marca);
		strcat(indice_marca[i].string, "\0");
		strcpy(indice_marca[i].pk, j.pk);
	}

	qsort(indice_marca, *nregistros, sizeof(Is), comp_isecondary);
}

/* Compara dois registros de indice de preco final e retorna o menor,
   usando a chave primaria como desempate
*/
int comp_iprice(const void *p1, const void *p2) {
	if ((*(Isf*)p1).price > (*(Isf*)p2).price) {
		return 1;
	} else if ((*(Isf*)p1).price < (*(Isf*)p2).price) {
		return -1;
	} else {
		return strcmp((*(Isf*)p1).pk, (*(Isf*)p2).pk);
	}
}

/*(Re)constroi indice de preco final dos produtos a partir do indice primario */
void criar_iprice(Isf *indice_preco, Ip *iprimary, int *nregistros) {
	Produto j;
	float preco;
	int desconto;

	for (int i = 0; i < *nregistros; i++) {
		if (iprimary[i].rrn != -1) {
			j = recuperar_registro(iprimary[i].rrn);
			strcpy(indice_preco[i].pk, j.pk);
			sscanf(j.preco, "%f", &preco);
			sscanf(j.desconto, "%d", &desconto);
			preco = preco *  (100-desconto)/(float)100;
			preco = preco * 100;
			preco = ((int) preco)/ (float) 100;
			indice_preco[i].price = preco;
		}
	}

	qsort(indice_preco, *nregistros, sizeof(Isf), comp_iprice);
}

/* Construtor da lista. */
void inicializar_lista(ll **head) {
    *head = NULL;
}

/* Insere um novo elemento na lista.
   Retorna 1 se inserido ou 0 se memoria insuficiente
*/
int inserir_lista(char *chave, ll **head) {
    ll *novo = (ll*) malloc(sizeof(ll));
    if (!novo)
        return 0;

    strcpy(novo->pk, chave);

    if (*head == NULL || strcmp(novo->pk, (*head)->pk) < 0) {
        novo->prox = *head;
        *head = novo;
    } else {
        ll *aux = *head;
        while (aux->prox && strcmp(aux->prox->pk, chave) < 0) {
            aux = aux->prox;
        }
        novo->prox = aux->prox;
        aux->prox = novo;
    }

    return 1;
}

/* Destroi lista e libera memoria */
void destruir_lista(ll **head) {
	ll *aux = *head, *temp;
	*head = NULL;
	while (aux) {
		temp = aux->prox;
		free(aux);
		aux = temp;
	}
}

/* Compara dois registros de indice de categoria e retorna o menor
*/
int comp_icategory(const void *p1, const void *p2) {
	return strcmp((*(Ir*)p1).cat, (*(Ir*)p2).cat);
}

/* (Re)constroi indice de categorias dos produtos a partir do indice primario */
void criar_icategory(Ir *indice_categoria, Ip *iprimary, int *nregistros, int *ncat) {
	Produto j;
	Ir *p;
	char *temp;

	*ncat = 0;

	// Percorre todos os registros do indice primario
	for (int i = 0; i < *nregistros; i++) {
		j = recuperar_registro(iprimary[i].rrn);
		
		temp = strtok(j.categoria, "|");

		// Percorre todas as categorias do registro
		while (temp) {
			p = buscar_categoria(indice_categoria, temp, *ncat);
			
			/* Registro de categoria nao existe. Cria uma nova lista invertida para essa 
				categoria */
			if (!p) {
				/* Se houver alguma outra lista naquela posicao, destroi lista e libera a memoria
				   antes de sobrescrever
				*/
				if (indice_categoria[*ncat].lista) {
					destruir_lista(&(indice_categoria[*ncat].lista));
				}
				inicializar_lista(&(indice_categoria[*ncat].lista));
				strcpy(indice_categoria[*ncat].cat, temp);
				inserir_lista(j.pk, &(indice_categoria[*ncat].lista));
				(*ncat)++;
			// Registro de categoria existe. Insere na lista invertida dessa categoria
			} else {
				inserir_lista(j.pk, &(p->lista));
			}

			temp = strtok(NULL, "|");
		}
	}

	qsort(indice_categoria, *ncat, sizeof(Ir), comp_icategory);
}

/* Cadastra um novo produto no arquivo de dados e atualiza os indices */
void cadastrar(Ip *iprimary, Is *iproduct, Is *ibrand, Isf *iprice, Ir *icategory, int *nregistros, int *ncat) {
	char registro[TAM_REGISTRO + 1] = {'\0'}, *temp, aux_cat[TAM_CATEGORIA + 1] = {'\0'};
	Produto j;
	int count = 0;

	if (*nregistros >= MAX_REGISTROS)
		return;

	scanf("%[^\n]%*c", j.nome);
	scanf("%[^\n]%*c", j.marca);
	scanf("%[^\n]%*c", j.data);
	scanf("%[^\n]%*c", j.ano);
	scanf("%[^\n]%*c", j.preco);
	scanf("%[^\n]%*c", j.desconto);
	scanf("%[^\n]%*c", j.categoria);
	gerarChave(&j);

	strcpy(aux_cat, j.categoria);
	strcat(aux_cat, "\0");

	temp = strtok(aux_cat, "|");
	while (temp) {	
		count++;
		temp = strtok(NULL, "|");
	}

	if (count + *ncat >= MAX_CATEGORIAS)
		return;

	Ip *busca = bsearch(j.pk, iprimary, *nregistros, sizeof(Ip), comp_iprimary);
	
	// Se chave primaria ja existir
	if (busca) {
		// Se registro nao estiver apagado, retorna mensagem de chave repetida
		if (busca->rrn != -1) {
			printf(ERRO_PK_REPETIDA, j.pk);
			return;
		// Se registro estiver apagado, atualiza o rrn
		} else {
			busca->rrn = *nregistros;
		}
	}

	registro[0] = '\0';

	strcat(registro, j.nome);
	strcat(registro, "@");
	strcat(registro, j.marca);
	strcat(registro, "@");
	strcat(registro, j.data);
	strcat(registro, "@");
	strcat(registro, j.ano);
	strcat(registro, "@");
	strcat(registro, j.preco);
	strcat(registro, "@");
	strcat(registro, j.desconto);
	strcat(registro, "@");
	strcat(registro, j.categoria);
	strcat(registro, "@");

	int tam = strlen(registro);
	for (int i = 0; i < (TAM_REGISTRO - tam); i++) {
		strcat(registro, "#");
	}

	strcat(ARQUIVO + (TAM_REGISTRO * *nregistros), registro);

	// Se o registro eh duplicado, nao necessita de atualizacao nos indices e incremento do nregistros
	if (busca)
		return;

	/* --- Atualiza indices --- */
	/* Índice primário */
	iprimary[*nregistros].rrn = *nregistros;
	strcpy(iprimary[*nregistros].pk, j.pk);
	qsort(iprimary, *nregistros + 1, sizeof(Ip), comp_iprimary);

	/* Indice para nome ou modelo dos produtos */
	strcpy(iproduct[*nregistros].pk, j.pk);
	strcpy(iproduct[*nregistros].string, j.nome);
	qsort(iproduct, *nregistros + 1, sizeof(Is), comp_isecondary);

	/* Indice para marca dos produtos */
	strcpy(ibrand[*nregistros].pk, j.pk);
	strcpy(ibrand[*nregistros].string, j.marca);
	qsort(ibrand, *nregistros + 1, sizeof(Is), comp_isecondary);

	/* Indice para categorias dos produtos */
	temp = strtok(j.categoria, "|");
	Ir *p;
	while (temp) {
		p = buscar_categoria(icategory, temp, *ncat);	
		/* Registro de categoria nao existe. Cria uma nova lista invertida para essa 
			categoria */
		if (!p) {
			/* Se houver alguma outra lista naquela posicao, destroi lista e libera a memoria
				antes de sobrescrever
			*/
			if (icategory[*ncat].lista) {
				destruir_lista(&(icategory[*ncat].lista));
			}
			inicializar_lista(&(icategory[*ncat].lista));
			strcpy(icategory[*ncat].cat, temp);
			inserir_lista(j.pk, &(icategory[*ncat].lista));
			(*ncat)++;
		// Registro de categoria existe. Insere na lista invertida dessa categoria
		} else {
			inserir_lista(j.pk, &(p->lista));
		}
		temp = strtok(NULL, "|");
	}
	qsort(icategory, *ncat, sizeof(Ir), comp_icategory);

	/* Indice para preço final dos produtos */
	float preco;
	int desconto;
	strcpy(iprice[*nregistros].pk, j.pk);
	sscanf(j.preco, "%f", &preco);
	sscanf(j.desconto, "%d", &desconto);
	preco = preco *  (100-desconto)/(float) 100;
	preco = preco * 100;
	preco = ((int) preco)/ (float) 100 ;
	iprice[*nregistros].price = preco;
	qsort(iprice, *nregistros + 1, sizeof(Isf), comp_iprice);

	(*nregistros)++;
}

/* Lista os registros formatados e ordenados por: (1) código, (2) categoria, (3) marca
   (4) preco com desconto aplicado
*/
void listar(Ip* iprimary, Is* iproduct, Is* ibrand, Ir* icategory, Isf *iprice, int nregistros, int ncat) {
	int opcao, nao_vazio = 0;
	ll *aux;
	Ip *pk_aux;
	Ir *cat_aux;
	char categoria[TAM_CATEGORIA + 1];

	scanf("%d%*c", &opcao);

	switch (opcao) {
		// Listar por codigo
		case 1:
			for (int i = 0; i < nregistros; i++) {
				// Se registro nao estiver apagado
				if (iprimary[i].rrn != -1) {
					exibir_registro(iprimary[i].rrn, 0);
					nao_vazio = 1;
					if (i < nregistros - 1)
						printf("\n");
				}
			}
			break;
		
		// Listar os produtos que possuem a categoria solicitada
		case 2:
			scanf("%[^\n]", categoria);
			// Recupera lista invertida da categoria
			cat_aux = buscar_categoria(icategory, categoria, ncat);
			if (!cat_aux) {
				printf(REGISTRO_N_ENCONTRADO);
				return;
			}
			
			// Primeiro elemento da lista
			aux = cat_aux->lista;
			
			// Percorre a lista (circular)
			while (aux != NULL) {
				// Recupera o indice da chave primaria de cada no
				pk_aux = bsearch(aux->pk, iprimary, nregistros, sizeof(Ip), comp_iprimary);
				// Verifica se o registro nao esta apagado
				if (pk_aux->rrn != -1) {
					exibir_registro(pk_aux->rrn, 0);
					nao_vazio = 1;
					if (aux->prox != NULL)
						printf("\n");
				}
				aux = aux->prox;
			}

			break;
		
		// Lista por marca
		case 3:
			for (int i = 0; i < nregistros; i++) {
				pk_aux = bsearch(ibrand[i].pk, iprimary, nregistros, sizeof(Ip), comp_iprimary);
				// Se o registro nao estiver apagado
				if (pk_aux->rrn != -1) {
					exibir_registro(pk_aux->rrn, 0);
					nao_vazio = 1;
					if (i < nregistros - 1)
						printf("\n");
				}
			}
			break;
		
		// Lista por preco com desconto aplicado
		case 4:
			for (int i = 0; i < nregistros; i++) {
				pk_aux = bsearch(iprice[i].pk, iprimary, nregistros, sizeof(Ip), comp_iprimary);
			    // Se o registro nao estiver apagado
				if (pk_aux->rrn != -1) {
					// Exibe com desconto aplicado
					exibir_registro(pk_aux->rrn, 1);
					nao_vazio = 1;
					if (i < nregistros - 1)
						printf("\n");
				}
			}
			break;
	} // end switch

	if (!nao_vazio)
		printf(REGISTRO_N_ENCONTRADO);
}

/* Procura por registro com a chave desejada no indice de categorias.
   Retorna ponteiro para registro caso encontre ou NULL caso contrario.
*/
Ir *buscar_categoria(Ir *indice_categoria, char *chave, int ncat) {
	for (int i = 0; i < ncat; i++) {
		if (!strcmp(indice_categoria[i].cat, chave)) {
			return indice_categoria + i;
		}
	}

	return NULL;
}

/* Procura por registro com a chave desejada no indice de nome (ou modelo).
   Retorna ponteiro para registro caso encontre ou NULL caso contrario.
*/
Is *buscar_modelo(Is *iproduct, char *chave, int nregistros) {
	for (int i = 0; i < nregistros; i++) {
		if (!strcmp(iproduct[i].string, chave))
			return iproduct + i;
	}

	return NULL;
}

/* Procura por registro com a chave desejada no indice de marca.
   Retorna ponteiro para registro caso encontre ou NULL caso contrario.
*/
Is *buscar_marca(Is *ibrand, char *chave, int nregistros) {
	for (int i = 0; i < nregistros; i++) {
		if (!strcmp(ibrand[i].string, chave))
			return ibrand + i;
	}

	return NULL;
}

/* Busca um registro por: (1) código, (2) nome ou modelo ou (3) marca e categoria.
   Mostra o produto formatado na tela se for encontrado ou mensagem de registro
   nao encontrado caso contrario
*/
void buscar(Ip *iprimary, Is *iproduct, Is *ibrand, Ir *icategory, int nregistros, int ncat) {
	int opcao, achou = 0;
	char aux[TAM_NOME + 1] = {'\0'}, categoria[TAM_CATEGORIA + 1] = {'\0'}, temp[TAM_CATEGORIA + 1] = {'\0'};
	char *p_aux;
	Ip *p;
	Is *s;
	Produto prod;

	scanf("%d%*c", &opcao);

	switch(opcao) {
		case 1:
			scanf("%[^\n]%*c", aux);
			p = bsearch(aux, iprimary, nregistros, sizeof(Ip), comp_iprimary);
			if (p) {
				if (p->rrn != -1)
					exibir_registro(p->rrn, 0);
				else {
					printf(REGISTRO_N_ENCONTRADO);
				}
			} else {
				printf(REGISTRO_N_ENCONTRADO);
			}
			break;
		
		case 2:
			scanf("%[^\n]%*c", aux);
			s = buscar_modelo(iproduct, aux, nregistros);
			if (s) {
				while (!strcmp(s->string, aux)) {
					p = bsearch(s->pk, iprimary, nregistros, sizeof(Ip), comp_iprimary);
					if (p->rrn != -1) {
						if (achou)
							printf("\n");
						exibir_registro(p->rrn, 0);
						/* Indica que pelo menos um registro foi encontrado, para contornar o caso de encontrar 
						   apenas registros que ja foram apagados
						*/
						achou = 1;	
					}
					s++;
				}
			} else {
				printf(REGISTRO_N_ENCONTRADO);
			}

			if (s && achou == 0)
				printf(REGISTRO_N_ENCONTRADO);
			break;
	
		case 3:
			scanf("%[^\n]%*c", aux);
			scanf("%[^\n]%*c", categoria);
			s = buscar_marca(ibrand, aux, nregistros);
			if (s) {
				while (!strcmp((s)->string, aux)) {
					p = bsearch((s)->pk, iprimary, nregistros, sizeof(Ip), comp_iprimary);
					if (p->rrn != -1) {
						prod = recuperar_registro(p->rrn);
						strcpy(temp, prod.categoria);
						strcat(temp, "\0");
						p_aux = strtok(temp, "|");

						while (p_aux) {
							if (!strcmp(p_aux, categoria)) {
								if (achou == 1) {
									printf("\n");
								}
								exibir_registro(p->rrn, 0);
								achou = 1;
							}
							p_aux = strtok(NULL, "|");
						}
					}
					s++;
				}
			} else {
				printf(REGISTRO_N_ENCONTRADO);
			}

			if (s && !achou)
				printf(REGISTRO_N_ENCONTRADO);
	} // end switch
}

/* Altera o desconto de um produto, este deve ter tamanho de 3 bytes e valor entre 0 e 100 */
int alterar(Ip* iprimary, Isf* iprice, int *nregistros) {
	char chave[TAM_PRIMARY_KEY + 1] = {'\0'}, desconto[TAM_DESCONTO + 1] = {'\0'}, registro[TAM_REGISTRO + 1] = {'\0'}; 
	Ip *p_pk;

	scanf("%[^\n]%*c", chave);

	p_pk = bsearch(chave, iprimary, *nregistros, sizeof(Ip), comp_iprimary);

	if (!p_pk || p_pk->rrn == -1) {
		printf(REGISTRO_N_ENCONTRADO);
		return 0;
	}

	scanf("%[^\n]%*c", desconto);

	// Verificacoes de integridade
	int desconto_int = atoi(desconto);
	while (strlen(desconto) != 3 || desconto_int < 0 || desconto_int > 100) {
		printf(CAMPO_INVALIDO);
		scanf("%[^\n]%*c", desconto);
		desconto_int = atoi(desconto);
	}
	
	// Se passou das verificacoes, aplica a alteracao
	Produto j = recuperar_registro(p_pk->rrn);

	strcat(registro, j.nome);
	strcat(registro, "@");
	strcat(registro, j.marca);
	strcat(registro, "@");
	strcat(registro, j.data);
	strcat(registro, "@");
	strcat(registro, j.ano);
	strcat(registro, "@");
	strcat(registro, j.preco);
	strcat(registro, "@");
	strcat(registro, desconto);
	strcat(registro, "@");
	strcat(registro, j.categoria);
	strcat(registro, "@");

	int tam = strlen(registro);
	for (int i = 0; i < (TAM_REGISTRO - tam); i++) {
		strcat(registro, "#");
	}

	strncpy(ARQUIVO + (p_pk->rrn * TAM_REGISTRO), registro, TAM_REGISTRO);

	criar_iprice(iprice, iprimary, nregistros);

	return 1;
}

/* Remove um produto marcando -1 no indice primario, o registro continua no arquivo
   e na tabela de indices
*/
int remover(Ip* iprimary, int nregistros) {	
	char chave[TAM_PRIMARY_KEY + 1], *p_arq;
	Ip *pk;
	
	scanf ("%[^\n]%*c", chave);
	pk = bsearch(chave, iprimary, nregistros, sizeof(Ip), comp_iprimary);

	// Verifica se registro existe
	if (!pk || pk->rrn == -1) {
		printf(REGISTRO_N_ENCONTRADO);
		return 0;
	}

	// Marca o inicio do registro no arquivo
	p_arq = ARQUIVO + pk->rrn * TAM_REGISTRO;
	strncpy(p_arq, "*|", 2);

	// Marca -1 no indice primario
	pk->rrn = -1;
	
	return 1;
}

// Libera o espaco em disco do arquivo de dados
void liberar_espaco(Ip *iprimary, Is* iproduct, Is *ibrand, Isf *iprice, Ir *icategory, int *nregistros, int *ncat) {
	char *esq, *dir;
	int flag = 0;

	/* ajustar nregistros ao numero de registros no arquivo de dados (considerando os duplicados)
	   no final, o nregistros ficara em conformidade com o numero de registros nos indices
	*/
	*nregistros = strlen(ARQUIVO) / TAM_REGISTRO;

	/* apontador esq e dir percorrem o arquivo e copia o registro de dir em esq, quando dir encontra
	   um registro para ser apagado, desloca para o proximo registro valido e continua a copia do arquivo
	*/

	esq = dir = ARQUIVO;

	while (*dir != '\0') {
		if (!strncmp(dir, "*|", 2)) {
			// procura o proximo registro valido
			while (!strncmp(dir, "*|", 2)) {
				dir += TAM_REGISTRO;
				(*nregistros)--;		
			}
			// indica que o arquivo precisa ser alterado, "arrastando" os registros para tras
			flag = 1;
		}

		if (flag && *dir != '\0') {
			strncpy(esq, dir, TAM_REGISTRO);
		}

		dir += TAM_REGISTRO;
		esq += TAM_REGISTRO;
	}
	ARQUIVO[*nregistros * TAM_REGISTRO] = '\0';

	/* Recriar indices */
	/* Índice primário */
  	if (!iprimary) {
		perror(MEMORIA_INSUFICIENTE);
		exit(1);
	}
	criar_iprimary(iprimary, nregistros);

	/* Indice para nome ou modelo dos produtos */
	if (!iproduct) {
		perror(MEMORIA_INSUFICIENTE);
		exit(1);
	}
	criar_iproduct(iproduct, iprimary, nregistros);

	/* Indice para marca dos produtos */
	if (!ibrand) {
		perror(MEMORIA_INSUFICIENTE);
		exit(1);
	}
	criar_ibrand(ibrand, iprimary, nregistros);

	/* Indice para categorias dos produtos */
	if (!icategory) {
		perror(MEMORIA_INSUFICIENTE);
		exit(1);
	}
	criar_icategory(icategory, iprimary, nregistros, ncat);

	/* Indice para preço final dos produtos */
	if (!iprice) {
		perror(MEMORIA_INSUFICIENTE);
		exit(1);
	}
	criar_iprice(iprice, iprimary, nregistros);
}

/* Libera a memoria principal e encerra o programa*/
void finalizar(Ip *iprimary, Is *iproduct, Is *ibrand, Isf *iprice, Ir *icategory, int ncat) {
	free(iprimary);
	free(iproduct);
	free(ibrand);
	free(iprice);

	for (int i = 0; i < ncat; i++) {
		destruir_lista(&(icategory[i].lista));
	}
	free(icategory);
}