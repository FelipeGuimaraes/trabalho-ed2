#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAM_PRIMARY_KEY	11


typedef struct linked_list{
  char pk[TAM_PRIMARY_KEY];
  struct linked_list *prox;
} ll; 

/* Construtor da lista.
   Retorna 1 se lista construida ou 0 se memoria insuficiente 
*/
void inicializar_lista(ll **head) {
    *head = NULL;
}

void destruir_lista(ll **head) {
	ll *aux = *head, *temp;
	*head = NULL;
	while (aux) {
		temp = aux->prox;
		free(aux);
		aux = temp;
	}
}

/* Insere um novo elemento na lista.
   Retorna 1 se inserido ou 0 se memoria insuficiente
*/
int inserir_lista(char *chave, ll **head) {
    ll *novo = (ll*) malloc(sizeof(ll));
    if (!novo)
        return 0;

    strcpy(novo->pk, chave);

    if (*head == NULL || strcmp(novo->pk, (*head)->pk) < 0) {
        novo->prox = *head;
        *head = novo;
    } else {
        ll *aux = *head;
        while (aux->prox && strcmp(aux->prox->pk, chave) < 0) {
            aux = aux->prox;
        }
        novo->prox = aux->prox;
        aux->prox = novo;
    }

    return 1;
}

/* Busca um elemento da lista e coloca-o em "resultado".
   Retorna 1 se elemento encontrado ou 0 caso contrario
*/
int busca(char *chave, ll *head, ll *resultado) {
    strcpy(head->pk, chave);
    ll *aux = head->prox;
    
    while (strcmp(aux->pk, head->pk)) {
        aux = aux->prox;
    }

    if (aux != head) {
    printf("passou krl\n");
        *resultado = *aux;
        return 1;
    } else {
        return 0;
    }
}

/* Remove um elemento da lista.
   Retorna 1 se elemento removido ou 0 se elemento nao existir
*/
int remover_lista(char *chave, ll **head) {
    // Lista vazia
    if (!*head)
        return 0;

    // Remove primeiro elemento
    if (!strcmp((*head)->pk, chave)) {
        ll *temp = *head;
        *head = (*head)->prox;
        free(temp);
        return 1;
    }
    
    // Procura elemento para remover
    ll *aux = *head;

    while (aux->prox && strcmp(aux->prox->pk, chave)) {
        aux = aux->prox;
    }

    if (aux->prox) {
        ll *temp = aux->prox;
        aux->prox = aux->prox->prox;
        free(temp);
        return 1;
    } else {
        return 0;
    }
}


void printar(ll *head) {
    ll *aux = head;
    
    if (!aux) {
        printf("Lista vazia\n");
        return;
    }

    while (aux) {
        printf("%s\n", aux->pk);
        aux = aux->prox;
    }
}





int main() {
    ll *lista;

    inicializar_lista(&lista);

    inserir_lista("alo", &lista);
    inserir_lista("isso", &lista);
    inserir_lista("eh", &lista);
    inserir_lista("um", &lista);
    inserir_lista("teste", &lista);

    printar(lista);
    printf("\n");

    remover_lista("isso", &lista);
    remover_lista("alo", &lista);
    remover_lista("um", &lista);
    printar(lista);
    destruir_lista(&lista);
    printar(lista);
    return 0;
}